window.$ = window.jQuery = require("jquery");
require('bootstrap');
require('slick-slider');
require('owl.carousel');

$(document).ready(function() {
  AOS.init();
  $('.help-select').select2({
    minimumResultsForSearch: Infinity
  });

 

});
$('.burger-menu-btn').click(function (e) {
    e.preventDefault();
    $(this).toggleClass('burger-menu-lines-active');
    $('.nav-panel-mobil').toggleClass('nav-panel-mobil-active');
    $('body').toggleClass('body-overflow');
  });
  

  $('.project-slider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    nextArrow: '<img src="../images/arrow-next.png" alt="" class="slick-next-btn">',
    prevArrow: '<img src="../images/arrow-prev.png" alt="" class="slick-prev-btn">',

  });
  
  $('.team-slider').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    nextArrow: '<img src="../images/arrow-3.png" alt="" class="slick-next-btn">',
    prevArrow: '<img src="../images/arrow-2.png" alt="" class="slick-prev-btn">',
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  

  $('.partners-slider').slick({
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    nextArrow: '<img src="../images/arrow-3.png" alt="" class="slick-next-btn">',
    prevArrow: '<img src="../images/arrow-2.png" alt="" class="slick-prev-btn">',
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

 
  $('.media-slider').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    nextArrow: '<img src="../images/arrow-3.png" alt="" class="slick-next-btn">',
    prevArrow: '<img src="../images/arrow-2.png" alt="" class="slick-prev-btn">',
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

        var banner = $('#banner');  
        var children = $('#help-children');
        banner.owlCarousel({
            loop: true,
            margin: 50,
            responsiveClass: true,
            dots: true,
            smartSpeed: 800,
            nav: false,
            items: 1
        });
        banner.owlCarousel({
            dotsContainer: '#carousel-custom-dots'
        });
        $('.owl-dot').click(function () {
            banner.trigger('to.owl.carousel', [$(this).index(), 300]);
            $('.owl-dot').removeClass('active');
            $(this).addClass('active');
        });
        
        children.owlCarousel({
            loop: true,
            margin: 30,
            responsiveClass: true,
            dots: true,
            smartSpeed: 800,
            nav: false,
            responsive:{
                0:{
                    items: 1
                },
                600:{
                    items: 3
                },
                1000:{
                    items: 4
                }
            }
        });
        $('#nav-prev').click(function(e) {
            e.preventDefault();
            children.trigger('prev.owl.carousel', [300]);
        });
        $('#nav-next').click(function(e) {
            e.preventDefault();
            children.trigger('next.owl.carousel');
        });



// $('.price-info').click(function(){
//   $(this).attr('disabled')
// })\

$(document).ready(function(){
  let priceInfos = document.querySelectorAll('.price-info');
  let wantHelpBtn = document.querySelector('.want-help-button');
  priceInfos.forEach(info => info.addEventListener('click', function() {
    if (wantHelpBtn.hasAttribute('disabled')) {
      wantHelpBtn.removeAttribute('disabled')
    }
  }))
});

// Pagination in page helps.php
$('.paginate').slick({
  infinite: false,
  dots: true,
  arrows: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  prevArrow: $('.prev_paginate'),
  nextArrow: $('.next_paginate'),
});

let help_modal = document.querySelector('.project-help')
let closeModal = document.querySelector('.closeModal')

help_modal.addEventListener('click', () => {
  $('#helpModal').modal('show')
})

closeModal.addEventListener('click', () => {
  $('#helpModal').modal('hide')
})
