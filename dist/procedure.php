<?php include 'header.php' ?>
<div class="how-help procedure">
    <div class="container">
        <div class="title" data-aos="fade-right" data-aos-duration='1100'>
            <h1>ПОРЯДОК ДЕЙСТВИЙ</h1>
            <p>Пожалуйста, внимательно прочитайте каждый пункт
                порядка действий для получения помощи</p>
        </div>
        <div class="procedure-inner">
            <div class="row">
                <div class="col-xl-8 col-md-7">
                    <div class="procedure-left">
                        <div class="procedure-title" data-aos="fade-right" data-aos-duration='1100'>
                            <div class="procedure-num">
                                <p>1</p>
                            </div>
                            <div class="procedure-name">
                                <h1>Письмо-обращение</h1>
                            </div>
                        </div>
                        <div class="procedure-text" data-aos="fade-right" data-aos-duration='1400'>
                            <p>Для получения помощи вам необходимо направить в фонд письмо-обращение в формате Word и
                                его отсканированную копию с подписью. К письму необходимо приложить фотографии вашего
                                ребенка горизонтального расположения в хорошем качестве, счет на оплату, последнюю
                                выписку из истории болезни вашего ребенка, заполненный бланк согласия на обработку
                                данных.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-5">
                    <div class="procedure-right">
                        <div class="download download-top" data-aos="fade-left" data-aos-duration='1100'>
                            <a href=""><img src="images/icon.png" alt="">Скачать образец письма</a>
                        </div>
                        <div class="download download-bottom" data-aos="fade-left" data-aos-duration='1400'>
                        <label for="file-upload" class="custom-file-upload">
                            <img src="images/download.png" alt="">Загрузить файл
                        </label>
                        <input id="file-upload" type="file" />
                        <button class="download_btn">Отправить</button>
                        </div>
                    </div>
                </div>
                <div class="col-xl-8 col-md-7">
                    <div class="procedure-left">
                        <div class="procedure-title" data-aos="fade-right" data-aos-duration='1100'>
                            <div class="procedure-num">
                                <p>2</p>
                            </div>
                            <div class="procedure-name">
                                <h1>Сбор документов</h1>
                            </div>
                        </div>
                        <div class="procedure-text" data-aos="fade-right" data-aos-duration='1400'>
                            <p>Если Вы получили положительный ответ от фонда, для оформления помощи Вам необходимо
                                предоставить следующие документы</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-5">
                    <div class="procedure-right">
                        <div class="download download-top" data-aos="fade-left" data-aos-duration='1100'>
                            <a href=""><img src="images/icon.png" alt="">Для полных семей</a>
                        </div>
                        <div class="download download-top" data-aos="fade-left" data-aos-duration='1400'>
                            <a href=""><img src="images/icon.png" alt="">Для неполных семей</a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-12">
                    <div class="procedure-left">
                        <div class="procedure-title" data-aos="fade-right" data-aos-duration='1100'>
                            <div class="procedure-num">
                                <p>3</p>
                            </div>
                            <div class="procedure-name">
                                <h1>Заключение договора</h1>
                            </div>
                        </div>
                        <div class="procedure-text" data-aos="fade-right" data-aos-duration='1400'>
                            <p>Если Вы получили положительный ответ от фонда, для оформления помощи Вам необходимо
                                предоставить следующие документы</p>
                        </div>
                        <div class="procedure-text-bottom">
                            <div class="row">
                                <div class="col-xl-6 col-md-6" data-aos="fade-right" data-aos-duration='1200'>
                                    <p><b>Для закупки оборудования:</b></p>
                                    <ul>
                                        <li> Утверждение договора купли-продажи</li>
                                        <li>Подписание договора пожертвования</li>
                                        <li> Подписание договора купли-продажи</li>
                                    </ul>
                                </div>
                                <div class="col-xl-6 col-md-6" data-aos="fade-left" data-aos-duration='1200'>
                                    <p><b>Для отправления на лечение:</b></p>
                                    <p>Фонд направляет Вам по почте два экземпляра договора пожертвования. Один из
                                        экземпляров необходимо подписать и отправить обратно в фонд. Второй эксземпляр
                                        остается Вам.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-12">
                    <div class="procedure-left">
                        <div class="procedure-title" data-aos="fade-right" data-aos-duration='1100'>
                            <div class="procedure-num">
                                <p>4</p>
                            </div>
                            <div class="procedure-name">
                                <h1>Завершающий этап</h1>
                            </div>
                        </div>
                        <div class="procedure-text-bottom">
                            <div class="row">
                                <div class="col-xl-6 col-md-6" data-aos="fade-right" data-aos-duration='1200'>
                                    <p><b>После получения товара:</b></p>
                                    <ul>
                                        <li> Подписание акта сдачи-приемки пожертвования</li>
                                        <li>Сбор документов (товарная накладная, счет-фактура)</li>
                                        <li> Предоставление фотографий полученных товаров</li>
                                    </ul>
                                </div>
                                <div class="col-xl-6 col-md-6" data-aos="fade-left" data-aos-duration='1200'>
                                    <p><b>По завершению курса лечения:</b></p>
                                    <ul>
                                        <li> Подписание акта сдачи-приемки пожертвования</li>
                                        <li>Предоставление фотографий, снятых во время прохождения курса лечения</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include 'footer.php' ?>