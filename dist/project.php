<?php include 'header.php' ?>
<div class="project">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Главная </a></li>
                <li class="breadcrumb-item active" aria-current="page">Проекты</li>
            </ol>
        </nav>
        <div class="title" data-aos='fade-right' data-aos-duration='1100'>
            <h1>Проекты</h1>
        </div>
        <div class="sub-title" data-aos='fade-right' data-aos-duration='1100'>
            <h1>ПРОГРАММА «Помощи детям с ДЦП в лечении и реабилитации»</h1>
        </div>
        <div class="project-inner-top">
            <div class="row">
                <div class="col-xl-4 col-md-6">
                    <a href="project-inner.php">
                        <div class="project-link" style="background-image: url(images/project1.png);" data-aos="zoom-in-right" data-aos-duration='1100'>
                            <h1>Проект <br> “Charity Box”</h1>
                        </div>
                    </a>
                </div>
                <div class="col-xl-4 col-md-6">
                    <a href="project-inner.php">
                        <div class="project-link" style="background-image: url(images/project1.png);" data-aos="zoom-in" data-aos-duration='1200'>
                            <h1>Проект <br> “Charity Box”</h1>
                        </div>
                    </a>
                </div>
                <div class="col-xl-4 col-md-6">
                    <a href="project-inner.php">
                        <div class="project-link" style="background-image: url(images/project1.png);" data-aos="zoom-in-left" data-aos-duration='1300'>
                            <h1>Проект <br> “Charity Box”</h1>
                        </div>
                    </a>
                </div>
                <div class="col-xl-4 col-md-6">
                    <a href="project-inner.php">
                        <div class="project-link" style="background-image: url(images/project1.png);" data-aos="zoom-in-right" data-aos-duration='1100'>
                            <h1>Проект <br> “Charity Box”</h1>
                        </div>
                    </a>
                </div>
                <div class="col-xl-4 col-md-6">
                    <a href="project-inner.php">
                        <div class="project-link" style="background-image: url(images/project1.png);" data-aos="zoom-in" data-aos-duration='1200'>
                            <h1>Проект <br> “Charity Box”</h1>
                        </div>
                    </a>
                </div>
                <div class="col-xl-4 col-md-6">
                    <a href="project-inner.php">
                        <div class="project-link" style="background-image: url(images/project1.png);" data-aos="zoom-in-left" data-aos-duration='1300'>
                            <h1>Проект <br> “Charity Box”</h1>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="project-inner-bottom">
    <div class="container">
        <div class="sub-title" data-aos="fade-right" data-aos-duration='1100'>
            <h1>ПРОГРАММА «Популяризации благотворительности и сбор добровольных пожертвований для осуществления
                благотворительной деятельности»</h1>
                <div class="row">
                    <div class="col-xl-4 col-md-6">
                        <a href="project-inner.php">
                            <div class="project-link" style="background-image: url(images/project1.png);" data-aos="zoom-in-right" data-aos-duration='1100'>
                                <h1>Проект <br> “Charity Box”</h1>
                            </div>
                        </a>
                    </div>
                    <div class="col-xl-4 col-md-6">
                        <a href="project-inner.php">
                            <div class="project-link" style="background-image: url(images/project1.png);" data-aos="zoom-in" data-aos-duration='1200'>
                                <h1>Проект <br> “Charity Box”</h1>
                            </div>
                        </a>
                    </div>
                    <div class="col-xl-4 col-md-6">
                        <a href="project-inner.php">
                            <div class="project-link" style="background-image: url(images/project1.png);" data-aos="zoom-in-left" data-aos-duration='1300'>
                                <h1>Проект <br> “Charity Box”</h1>
                            </div>
                        </a>
                    </div>
                    <div class="col-xl-4 col-md-6">
                        <a href="project-inner.php">
                            <div class="project-link" style="background-image: url(images/project1.png);" data-aos="zoom-in-right" data-aos-duration='1100'>
                                <h1>Проект <br> “Charity Box”</h1>
                            </div>
                        </a>
                    </div>
                    <div class="col-xl-4 col-md-6">
                        <a href="project-inner.php">
                            <div class="project-link" style="background-image: url(images/project1.png);" data-aos="zoom-in" data-aos-duration='1200'>
                                <h1>Проект <br> “Charity Box”</h1>
                            </div>
                        </a>
                    </div>
                    <div class="col-xl-4 col-md-6">
                        <a href="project-inner.php">
                            <div class="project-link" style="background-image: url(images/project1.png);" data-aos="zoom-in-left" data-aos-duration='1300'>
                                <h1>Проект <br> “Charity Box”</h1>
                            </div>
                        </a>
                    </div>
                </div>
        </div>
    </div>
</div>
<div class="all-project">
    <div class="container">
        <a href="">Смотреть все программы <img src="images/arrow-project.png" alt=""></a>
    </div>
</div>
<?php include 'footer.php' ?>
