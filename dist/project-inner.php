<?php include 'header.php' ?>
<div class="project-inner">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Главная </a></li>
                <li class="breadcrumb-item active" aria-current="page">Проекты</li>
            </ol>
        </nav>
        <div class="title">
            <h1>Проекты</h1>
        </div>
        <div class="project-inner-title">
            <h2>Прошел 2-й и 3-й мастер-класс в рамках
                проекта внедрения программы «MOVE» в Елизаветинском саду</h2>
            <strong>Sep 8 2020</strong>
        </div>
        <div class="project-info">
            <div class="project-image">
                <img src="images/project-inner.png" alt="">
            </div>
            <div class="project-text">
                <p>В первой половине марта при поддержке фонда «Добросердие» прошли второй и третий мастер – классы
                    в рамках проекта внедрения программы «MOVE» («MOVING OPPORTUNITIES VIA EDUCATION (MOVE)» –
                    Движение для жизни) в систему комплексного сопровождения семьи в группе дневного пребывания
                    «Елизаветинский сад» (АНО «ММДЦ»).</p>
                <p>Специалисты центра и эксперты обсудили актуальные вопросы физической и умственной абилитации
                    детей раннего возраста для профилактики или минимизации инвалидности. Много внимания было
                    уделено теме социальной адаптации детей с выраженными ограничениями двигательных возможностей, с
                    применением ТСР. Обсудили технологию и приемы работы со специальным оборудованием, повышающем
                    эффективность социально — бытовой реабилитации.</p>
                <p>В центре много различного реабилитационного оборудования, которое поставлялось в разное время,
                    разными поставщиками. Сотрудники центра пользовались оборудованием по назначению, но порой
                    просто не знали, насколько разнообразны возможности этой техники. Как можно настроить ходунки,
                    вертикализатор или кресло не просто под конкретного ребенка, но под определенные
                    реабилитационные задачи. Ведь главная цель реабилитации — максимально развить функциональные
                    возможности ребенка в области самообеспечения, а наиболее эффективна реабилитация та, которая
                    включена в повседневную жизнь.</p>
                <div class="project-video">
                    <iframe width="100%" height="392" src="https://www.youtube.com/embed/stVysZmRFBo" frameborder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
                </div>
                <div class="project-text">
                    <p>На занятиях разбирались примеры из повседневной деятельности детей и специалистов центра.
                        Педагоги и воспитатели смогли обсудить с экспертами вопросы, накопившиеся в работе с детьми
                        и их родителями.
                    </p>
                    <p>Вот несколько отзывов участников:</p>
                    <div class="project-text-inner">
                        <p>Воспитатель Наталья: «…ценная информация для меня о пассивной и активной позе в
                            коляске — это важно очень! Приоритет отдается вертикализации ребенка, возможности вести
                            в этом смысле полноценный образ жизни».</p>
                        <p>Приоритет отдается вертикализации ребенка, возможности вести в этом смысле полноценный
                            образ жизни». Татьяна Сергеевна: «Спасибо за маленькие детали! Я обращаю внимание на
                            большие детали — какая коляска, как держится спина, не запрокинута ли голова и совсем
                            забываю, что при разведении ног сидя нужно обращать внимание на маленькие ступни, ведь
                            они части целой большой ноги. На каждом обучении мне интересно слушать и обсуждать свой
                            и чужой опыт. Каждый раз у меня как будто глаза открываются. После обучения осознаёшь,
                            что все это чувствовал и даже что-то делал из этого, то ли не осознанно, то ли по
                            каким-то другим причинам. И это очень интересно анализировать свои действия.»</p>
                        <p>Воспитатель Ольга: «Я делаю очень много открытий для себя на занятиях: и шаг под
                            наклоном при переносе веса, и вертикализация не менее 4 часов в день. Что особенно
                            поразило, это то огромное и необходимое количество повторений для отработки одного
                            навыка!».</p>

                    </div>
                    <div class="project-text">
                        <p>Удивительный эффект произвели на участников мастер-классов короткие видеосюжеты, с
                            консультацонно-практических занятий по адаптированной программе «МУВ (мобильность,
                            уверенность, взаимодействие) – движение для жизни». Эти занятия проводятся
                            экспертами программы MOVE в семьях, воспитывающих детей-инвалидов, участников
                            программы длительной поддержки фонда «Добросердие». Видео и фото показали реальное
                            усиление реабилитационного эффекта от программы при взаимодействии с семьей ребенка.
                        </p>
                    </div>
                </div>
                <div class="project-slider">
                    <div class="project-slide">
                        <img src="images/project-slide.png" alt="">
                    </div>
                    <div class="project-slide">
                        <img src="images/project-slide.png" alt="">
                    </div>
                    <div class="project-slide">
                        <img src="images/project-slide.png" alt="">
                    </div>
                </div>
                <div class="project-text">
                    <div class="project-text-inner">
                        <p>Отзыв специалиста центра: «Нам показали в ситуации реального занятия с семьей ребенка как
                            ставятся цели, задачи, как реагирует семья – принимает или не принимает их, как в ответ
                            реагирует консультант. А потом нам показали уже этот ВАУ-эффект! Вот, что было, а вот, что
                            стало после нескольких занятий по программе в сотрудничестве с семьей. Это реальный
                            материал, на реальных детях, в реальных семьях, эти видео и фото, они лучше всех слов
                            мотивируют нашу команду на работу по программе!».</p>
                    </div>
                </div>
                <div class="project-video">
                    <iframe width="100%" height="392" src="https://www.youtube.com/embed/stVysZmRFBo" frameborder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
                </div>
                <div class="project-help">
                    <a>Помочь проекту</a>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-lg" id="helpModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal_h2">
                        <h2>Хочу помочь</h2>
                        <img class="close closeModal" src="./images/close.svg" alt="">
                    </div>
                    <div class="how-help">
                    <div class="how-help-inner">
                    <div class="how-help-inputs">
                        <div class="help-input" data-aos="zoom-in" data-aos-duration='1200'>
                            <p>Сумма</p>
                            <input type="text" class="input-summ" placeholder="500">
                        </div>
                        <div class="help-input" data-aos="zoom-in" data-aos-duration='1400'>
                            <p>Имя</p>
                            <input type="text" class="input-name" placeholder="Евгений">
                        </div>
                        <div class="help-input" data-aos="zoom-in" data-aos-duration='1600'>
                            <p>E-mail</p>
                            <input type="text" class="input-email" placeholder="Valentina@gmail.com">
                        </div>
                    </div>
                    <div class="how-help-checkbox" data-aos="zoom-in" data-aos-duration='1700'>
                        <li class="list__item">
                            <label class="label--checkbox">
                                <input type="checkbox" class="checkbox" >
                                Осуществляя пожертвование, я принимаю условия договора
                            </label>
                        </li>
                    </div>
                    <div class="how-help-btn" data-aos="zoom-in" data-aos-duration='1500'>
                        <a href="">Сделать пожертвование</a>
                    </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include 'footer.php' ?>