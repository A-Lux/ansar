<?php include 'header.php' ?>
<div class="reports-page content">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Главная </a></li>
                <li class="breadcrumb-item active" aria-current="page">Отчеты</li>
            </ol>
        </nav>
        <div class="title" data-aos="fade-right" data-aos-duration='1100'>
            <h1>Отчеты</h1>
        </div>
        <h2 data-aos="fade-right" data-aos-duration='1200'>Благотворительный фонд <strong>«ДОБРОСЕРДИЕ»</strong> — российская некоммерческая организация, которая оказывает
            помощь в лечении и реабилитации детей <strong>с диагнозом детский церебральный паралич (ДЦП).</strong></h2>
        <div class="reports-info" data-aos="zoom-in" data-aos-duration='1300'>
            <p> <b>В период с 2008 по 2019 г.</b> при финансовой поддержке фонда 665 детей с церебральным параличом (ДЦП) прошли
                курсы лечения и/или реабилитации. Фонд оплатил расходы на приобретение технических средств реабилитации
                для 181 ребенка-инвалида. Материальную помощь на улучшение качества жизни получили <b>65 малоимущие семьи,</b>
                воспитывающие 1 или более детей с церебральным параличом. <b>339 детей</b> смогли принять участие
                в спортивных соревнованиях и сборах, из них <b>42 ребенка с ДЦП</b> и нарушениями в работе опорно-двигательного
                аппарата. <b>797 законных</b> представителя детей и специалисты некоммерческих организаций, помогающих детям с
                ДЦП, прошли курсы обучения, участвовали в семинарах, мастер-классах
                и лекториях. 23 медицинских учреждения получили специально оборудование. 21 некоммерческая организация
                получили материальную помощь
                на развитие собственных программ. <b>257 волонтеров</b> помогали фонду в различных мероприятиях. <b>292
                    бизнес-компаний</b> стали партнерами фонда
                в различных мероприятиях и проектах.</p>
        </div>
        <div class="title" data-aos="fade-right" data-aos-duration='1100'>
            <h3>Отчеты о расходах и деятельности фонда:</h3>
        </div>
        <div class="reports-inner">
            <div class="row">
                <div class="col-xl-3 col-md-6">
                    <div class="report-item" data-aos="zoom-in-down" data-aos-duration='1100'>
                        <div class="report-year">
                            <h1>2020</h1>
                        </div>
                        <div class="report-text">
                            <p>Отчет о расходах за <a href="">2020</a> год</p>
                            <p>Благодарим за помощь  в <a href="">2020</a> году</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="report-item" data-aos="zoom-in-down" data-aos-duration='1300'>
                        <div class="report-year">
                            <h1>2019</h1>
                        </div>
                        <div class="report-text">
                            <p>Отчет о расходах за <a href="">2019</a> год</p>
                            <p>Годовой отчет о деятельности в <a href="">2019</a> году</p>
                            <p>Благодарим за помощь  в <a href="">2020</a> году</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="report-item" data-aos="zoom-in-down" data-aos-duration='1500'>
                        <div class="report-year">
                            <h1>2018</h1>
                        </div>
                        <div class="report-text">
                            <p>Отчет о расходах за <a href="">2018</a> год</p>
                            <p>Годовой отчет о деятельности в <a href="">2018</a> году</p>
                            <p>Благодарим за помощь  в <a href="">2018</a> году</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="report-item" data-aos="zoom-in-down" data-aos-duration='1700'>
                        <div class="report-year">
                            <h1>2017</h1>
                        </div>
                        <div class="report-text">
                            <p>Отчет о расходах за <a href="">2017</a> год</p>
                            <p>Благодарим за помощь  в <a href="">2017</a> году</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="international-activity">
        <div class="container">
            <div class="title" data-aos="fade-right" data-aos-duration='1100'>
                <h3>Международная деятельность</h3>
            </div>
            <div class="row">
                <div class="col-xl-6 col-md-6">
                    <div class="international-image" data-aos="zoom-in-right" data-aos-duration='1100'>
                        <img src="images/reports1.png" alt="">
                    </div>
                    <div class="international-text" data-aos="zoom-in-right" data-aos-duration='1300'>
                        <h1>Благотворительная деятельность в ОАЭ</h1>
                        <p>Благотворительный фонд «Добросердие» начал свою работу 7 апреля 2008 г. За 11 лет работы фонда общая сумма средств, направленных 
                            на реализацию программ и проектов БФ «Добросердие» составила 90 487 020 руб.</p>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6">
                    <div class="international-image" data-aos="zoom-in-left" data-aos-duration='1100'>
                        <img src="images/reports2.png" alt="">
                    </div>
                    <div class="international-text" data-aos="zoom-in-left" data-aos-duration='1300'>
                        <h1>Благотворительная деятельность в ОАЭ</h1>
                        <p>Благотворительный фонд «Добросердие» начал свою работу 7 апреля 2008 г. За 11 лет работы фонда общая сумма средств, направленных 
                            на реализацию программ и проектов БФ «Добросердие» составила 90 487 020 руб.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'footer.php' ?>