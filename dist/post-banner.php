<?php include 'header.php' ?>
<div class="how-help">
    <div class="container">
        <div class="how-help-title" data-aos="zoom-in-right" data-aos-duration='1200'>
            <h1>РАЗМЕСТИТЬ НАШ БАННЕР</h1>
        </div>
        <div class="how-help-inner">
            <p data-aos="zoom-in-right" data-aos-duration='1300'>Благотворительный фонд «Добросердие» будет очень благодарен вам за размещение нашего баннера на вашем
                сайте.</p>
            <p data-aos="zoom-in-right" data-aos-duration='1300'>Пришлите нам ваши размеры на электронную почту: <a href="">info@dobroserdie.com</a> и мы сделаем баннер с нашим
                логотипом индивидуально для вашего сайта.</p>
        </div>
    </div>
</div>

<?php include 'footer.php' ?>