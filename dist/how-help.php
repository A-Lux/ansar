<?php include 'header.php' ?>
<div class="how-help">
    <div class="container">
        <div class="how-help-title" data-aos="zoom-in" data-aos-duration='1200'>
            <h1>Как Помочь?</h1>
        </div>
        <div class="requisites-info">
            <div class="row d-flex justify-content-center">
                <div class="col-xl-4 col-md-6 col-12">
                    <div class="requisite-inner" data-aos="zoom-in-right" data-aos-duration='1100'>
                        <div class="requisite-image">
                            <a href="bank-cards.php"><img src="images/bank-cards.png" alt=""></a>
                        </div>
                        <div class="requisite-title">
                            <a href="bank-cards.php">Банковские карты</a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-6 col-12">
                    <div class="requisite-inner" data-aos="zoom-in" data-aos-duration='1300'>
                        <div class="requisite-image">
                            <a href="kaspi.php"><img src="images/kaspi.png" alt=""></a>
                        </div>
                        <div class="requisite-title">
                            <a href="kaspi.php">Kaspi Gold</a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-6 col-12">
                    <div class="requisite-inner" data-aos="zoom-in-left" data-aos-duration='1500'>
                        <div class="requisite-image">
                            <a href="corporate-partnership.php"><img src="images/requisites.png" alt=""></a>
                        </div>
                        <div class="requisite-title">
                            <a href="corporate-partnership.php">Корпоративное <br>
                                партнерство</a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-6 col-12">
                    <div class="requisite-inner" data-aos="zoom-in-right" data-aos-duration='1600'>
                        <div class="requisite-image">
                            <a href="volunteer.php"><img src="images/volonter.png" alt=""></a>
                        </div>
                        <div class="requisite-title">
                            <a href="volunteer.php">Стать <br>
                                волонтером</a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-6 col-12">
                    <div class="requisite-inner" data-aos="zoom-in-left" data-aos-duration='1700'>
                        <div class="requisite-image">
                            <a href="post-banner.php"><img src="images/banner.png" alt=""></a>
                        </div>
                        <div class="requisite-title">
                            <a href="post-banner.php">Разместить наш  <br>
                                баннер</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'footer.php' ?>