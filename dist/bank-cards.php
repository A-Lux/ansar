<?php include 'header.php' ?>
<div class="how-help">
    <div class="container">
        <div class="how-help-title" data-aos="zoom-in" data-aos-duration='1200'>
            <h1>Банковские карты</h1>
        </div>
        <div class="how-help-inner" >
            <p data-aos="zoom-in" data-aos-duration='1200'>Пожертвования для</p>
            <div class="how-help-select" data-aos="zoom-in" data-aos-duration='1500'>
                <select class="help-select" name="state">
                    <option  selected>Выберите из списка...</option>
                    <option >Хирурга Михаила Куратовича</option>
                    <option >Консультация врача ортопеда</option>
                    <option >Саминов Артем</option>
                    <option >Карикарович Масикан</option>
                    <option >Кундашев Анатолий</option>
                    <option >Невропотолог Стас</option>
                    <option >Колина Мария</option>
                  </select>
            </div>
            <div class="how-help-inputs">
                <div class="help-input" data-aos="zoom-in" data-aos-duration='1200'>
                    <p>Сумма</p>
                    <input type="text" class="input-summ" placeholder="500">
                </div>
                <div class="help-input" data-aos="zoom-in" data-aos-duration='1400'>
                    <p>Имя</p>
                    <input type="text" class="input-name" placeholder="Евгений">
                </div>
                <div class="help-input" data-aos="zoom-in" data-aos-duration='1600'>
                    <p>E-mail</p>
                    <input type="text" class="input-email" placeholder="Valentina@gmail.com">
                </div>
            </div>
            <div class="how-help-checkbox" data-aos="zoom-in" data-aos-duration='1700'>
                <li class="list__item">
                    <label class="label--checkbox">
                        <input type="checkbox" class="checkbox" >
                        Осуществляя пожертвование, я принимаю условия договора
                    </label>
                  </li>
            </div>
            <div class="how-help-btn" data-aos="zoom-in" data-aos-duration='1500'>
                <a href="">Сделать пожертвование</a>
            </div>
        </div>
    </div>
</div>

<?php include 'footer.php' ?>