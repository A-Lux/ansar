<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-xl-4 col-md-4">
                <div class="footer-left">
                    <div class="write-btn">
                        <a href="">Написать нам</a>
                    </div>
                    <p>127006, Алматы, <br> ул. Красная, д. 16, стр. 6</p>
                    <div class="footer-contact">
                        <a href=""> +7 (499) 500-14-15,</a>
                        <a href="">info@life-line.ru</a>
                    </div>
                </div>
            </div>
            <div class="col-xl-8 col-md-8">
                <div class="footer-right">
                    <div class="footer-input">
                        <input type="text" placeholder="Поиск по сайту">
                    </div>
                    <div class="footer-links">
                        <div class="row">
                            <div class="col-xl-3 col-md-6">
                                <div class="footer-link">
                                    <a href="">Помощь детям</a>
                                    <a href="">Проекты</a>
                                </div>

                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="footer-link">
                                    <a href="">Отчеты</a>
                                    <a href="">Получить помощь</a>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="footer-link">
                                    <a href="">Новости</a>
                                    <a href="">Контакты</a>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="footer-link">
                                    <a href="">Разработано в</a>
                                    <a href=""><img src="images/logo-a-lux.png" alt=""></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-4">
                <div class="footer-left footer-left-bottom">
                   <div class="social-icons">
                       <a href="">
                        <i class="fab fa-instagram"></i>
                       </a>
                       <a href="">
                        <i class="fab fa-vk"></i>
                       </a>
                       <a href="">
                        <i class="fab fa-telegram-plane"></i>
                       </a>
                       <a href="">
                        <i class="fab fa-whatsapp"></i>
                       </a>
                   </div>
                </div>
            </div>
            <div class="col-xl-8 col-md-8">
                <div class="footer-right">
                    <div class="footer-bottom-info">
                        <p>Все права защищены (с) 2020</p>
                        <a href="">Политика конфиденциальности</a>
                        <a href="">Карта сайта</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="js/main.js"></script>
<script src="js/aos.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
</body>

</html>